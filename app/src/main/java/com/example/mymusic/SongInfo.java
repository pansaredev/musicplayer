package com.example.mymusic;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class SongInfo implements Parcelable {
    private String name;
    private String artist;
    private String songURL;

    public SongInfo(String name, String artist, String songURL) {
        this.name = name;
        this.artist = artist;
        this.songURL = songURL;
    }

    protected SongInfo(Parcel in) {
        name = in.readString();
        artist = in.readString();
        songURL = in.readString();
    }

    public static final Creator<SongInfo> CREATOR = new Creator<SongInfo>() {
        @Override
        public SongInfo createFromParcel(Parcel in) {
            return new SongInfo(in);
        }

        @Override
        public SongInfo[] newArray(int size) {
            return new SongInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(artist);
        dest.writeString(songURL);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getSongURL() {
        return songURL;
    }

    public void setSongURL(String songURL) {
        this.songURL = songURL;
    }

}
