package com.example.mymusic;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.rtp.AudioStream;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.LongDef;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.IOException;
import java.lang.ref.WeakReference;

public class MusicPlayerService extends Service {
    MediaPlayer mp ;
    Intent myIntent;
    AudioManager audioManager;
    AudioFocusRequest audioFocusRequest;
    AudioAttributes audioAttributes;
    AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener;
    int audioFocus;
    LocalBroadcastManager broadcastManager;
    private MusicServiceInterface listener;

    int currentVolume;

    boolean transientFocusLoss = false;
    boolean transientCanDuckFocusloss = false;
    boolean permanentFocusLoss = false;

    public void setListener(MusicServiceInterface listener) {
        this.listener = listener;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("MusicPlayerService","OnCreate music service called...");
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioAttributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();
        onAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange) {
                switch(focusChange){
                    case AudioManager.AUDIOFOCUS_GAIN:
                        if(transientFocusLoss) {
                            transientFocusLoss = false;
                            resumeMusic();
                        }
                        else if(transientCanDuckFocusloss){
                            transientCanDuckFocusloss=false;
                            resumeMusicWithPreviousVolumeLevel();
                        }else if(permanentFocusLoss){
                            permanentFocusLoss = false;
                        }

                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                        pauseMusic();
                        transientFocusLoss = true;
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                        reduceVolume();
                        transientCanDuckFocusloss = true;
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS:
                        permanentFocusLoss = true;
                        stopMusic();
                        stopSelf();
                        break;
                }
            }
        };
        audioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                .setAudioAttributes(audioAttributes)
                .setOnAudioFocusChangeListener(onAudioFocusChangeListener)
                .setAcceptsDelayedFocusGain(true)
                .build();
        audioFocus = audioManager.requestAudioFocus(audioFocusRequest);
        createNotificationChannel();
        mp= new MediaPlayer();
    }

    private void stopMusic() {
        if(mp.isPlaying()){
            mp.stop();
            mp.reset();
            mp.release();
        }
    }

    private void resumeMusicWithPreviousVolumeLevel() {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,currentVolume,0);
    }

    private void reduceVolume() {
        currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        double maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int reducedVolumeLevel = (int) (0.2*maxVolume);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,reducedVolumeLevel,0);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MusicPlayerService","onStartCommand called...\nSongURL : "+intent.getStringExtra("SongURL"));
        switch (audioFocus){
            case AudioManager.AUDIOFOCUS_REQUEST_GRANTED:
                broadcastManager = LocalBroadcastManager.getInstance(this);
                RemoteViews notificationLayout = new RemoteViews(getPackageName(),R.layout.notification_layout);
                myIntent = new Intent(this,MainActivity.class);
                PendingIntent pen = PendingIntent.getActivity(this,0,myIntent,0);


                Notification notification = new NotificationCompat.Builder(this,"songNotification")
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setCustomContentView(notificationLayout)
                        .setStyle(new androidx.media.app.NotificationCompat.DecoratedMediaCustomViewStyle())
                        .setContentIntent(pen)
                        .build();
                startForeground(101,notification);

                try {
                    playMusic(intent.getStringExtra("SongURL"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case AudioManager.AUDIOFOCUS_REQUEST_FAILED:
                break;
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void playMusic(String songURL) throws IOException {

        if(!mp.isPlaying()){
            mp.setDataSource(songURL);
            mp.prepare();
            mp.start();
            Log.d("MusciPlayerService","mp was not playing, started playback");
        }else{
            mp.stop();
            mp.reset();
            mp.setDataSource(songURL);
            mp.prepare();
            mp.start();
            Log.d("MusciPlayerService","mp was playing, reseted mp and starting playback");
        }
    }

    public void pauseMusic(){
        if(mp.isPlaying()){
            mp.pause();
            Log.d("MusciPlayerService","playback paused..");
        }
    }

    public void resumeMusic(){
        if(!mp.isPlaying()){
            mp.start();
            Log.d("MusciPlayerService","playback resumed..");
        }
    }

    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence channelName = "Channel1";
            String description = "Song Notification";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("songNotification",channelName,importance);
            channel.setDescription(description);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
            Log.d("MusicPlayerService","Channel created...");
        }
    }

    @Override
    public void onDestroy() {
        Log.d("MusicService","MusicService Destoyed");
        stopForeground(true);
        super.onDestroy();
    }

    public class MyBinder extends Binder{
        public MusicPlayerService getService(){
            return MusicPlayerService.this;
        }
    }

    public interface MusicServiceInterface{
        void onPauseClicked();
    }
}
