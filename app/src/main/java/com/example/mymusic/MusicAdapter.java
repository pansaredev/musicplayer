package com.example.mymusic;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MusicAdapter extends BaseAdapter {
    private ArrayList<SongInfo> songlist;
    private Listener listener;
    TextView songName;
    TextView artist;
    LinearLayout song;

    public MusicAdapter(ArrayList<SongInfo> songlist,
                        Listener listener) {
        this.songlist = songlist;
        this.listener = listener;

    }

    @Override
    public int getCount() {
        return songlist.size();
    }

    @Override
    public Object getItem(int position) {
        return songlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = listener.inflate(parent);
        songName = convertView.findViewById(R.id.song_name);
        artist = convertView.findViewById(R.id.song_artist);
        songName.setText(songlist.get(position).getName());
        artist.setText(songlist.get(position).getArtist());
        song = convertView.findViewById(R.id.card);
        song.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                //Start service
                Log.d("MainActivity","Clicked on :"+songlist.get(position).getSongURL());

                listener.playMusic(songlist.get(position),position);
            }
        });

        return convertView;
    }

    public interface Listener {
        void playMusic(SongInfo song, int position);
        View inflate(ViewGroup parent);
    }
}
