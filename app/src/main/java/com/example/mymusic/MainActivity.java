package com.example.mymusic;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MusicAdapter.Listener
        , MusicPlayerFragment.fragmentListener {
    private final String TAG = "MainActivity";
    ListView listView;
    SongInfo song;
    int position;
    private ArrayList<SongInfo> slist = new ArrayList<SongInfo>();
    MusicPlayerService musicService;

    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==491 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
            loadMusicFiles();
    }

    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            musicService =((MusicPlayerService.MyBinder) service).getService();
            musicService.setListener(new MusicPlayerService.MusicServiceInterface() {
                @Override
                public void onPauseClicked() {

                }
            });
            Log.d("MainActivity","Service Connected...");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG,"ServiceDisconnected");
            musicService = null;
        }
    };



    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MainActivity","OnCreate called");
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.music_listview);
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},491);
        }else{
            loadMusicFiles();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    private void loadMusicFiles(){
        Log.d("MainActivity","loadMusic called..");
        ContentResolver contentResolver = getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {
                    MediaStore.Audio.Media.TITLE,
                    MediaStore.Audio.Media.ARTIST,
                    MediaStore.Audio.Media.DATA
                };
        Cursor rs = contentResolver.query(uri,projection,null,null,null);

        if(rs!=null){
            while(rs.moveToNext()){
                String url = rs.getString(rs.getColumnIndex(MediaStore.Audio.Media.DATA));
                String name = rs.getString(rs.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String artist = rs.getString(rs.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                slist.add(new SongInfo(name,artist,url));
            }
        }
        rs.close();
        Adapter musicAdapter = new MusicAdapter(slist,this);
        listView.setAdapter((ListAdapter) musicAdapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void playMusic(SongInfo song, int position) {
        this.song = song;
        this.position = position;
        Bundle bundle = new Bundle();
        bundle.putParcelable("SongObj",song);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MusicPlayerFragment fragment = new MusicPlayerFragment(this);
        fragment.setArguments(bundle);
        ft.replace(R.id.content_main,fragment)
            .addToBackStack(null)
            .commit();
    }

    @Override
    public View inflate(ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.song_card,parent,false);
    }

    @Override
    public void pauseMusic() {
        musicService.pauseMusic();
    }

    @Override
    public void resumeMusic() {
        musicService.resumeMusic();
    }

    @Override
    public void onBackPressedFromFragment() {
        onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void startMusicPlayerService() {
        Intent intent = new Intent(this,MusicPlayerService.class);
        intent.putExtra("SongURL",song.getSongURL());
        startForegroundService(intent);
        bindService(intent,mConnection,Context.BIND_AUTO_CREATE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onNextClicked() {
        position++;
        song = slist.get(position);
        playMusic(song,position);
    }


    //lifecycle over-ridden methods below...
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity","OnStart called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity","OnStop called");
    }

    @Override
    protected void onDestroy() {
        Log.d("MainActivity","OnDestroy called");
        super.onDestroy();
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("MainActivity","OnRestoreInstanceState called");
    }

    @Override
    protected void onSaveInstanceState(@NonNull @NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("MainActivity","OnSaveInstanceState called");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity","OnPause called");
    }
}
