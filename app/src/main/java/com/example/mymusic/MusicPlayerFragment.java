package com.example.mymusic;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import org.jetbrains.annotations.NotNull;

public class MusicPlayerFragment extends Fragment {
    SongInfo currentSong;
    ImageButton backButton, playPause, next, prev;
    TextView songName, songArtist;
    boolean isplaying;
    fragmentListener listener ;

    public MusicPlayerFragment() {
        /* Required empty public constructor */
    }
    public MusicPlayerFragment(fragmentListener listener){
        this.listener = listener;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MusicPlayerFragment", "OnCreate Fragment..");
        if (getArguments() != null) {
            currentSong = getArguments().getParcelable("SongObj");
            listener.startMusicPlayerService();
            isplaying = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View V =  inflater.inflate(R.layout.fragment_music_player, null);
        backButton = V.findViewById(R.id.back_button);
        playPause = V.findViewById(R.id.play_pause);
        next = V.findViewById(R.id.next_button);
        prev = V.findViewById(R.id.prev_button);
        songName = V.findViewById(R.id.music_name);
        songName.setText(currentSong.getName());
        songArtist = V.findViewById(R.id.music_artist);
        songArtist.setText(currentSong.getArtist());

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MusicPlayerFragment","Clicked on back_button OnBackPressed...");
                listener.onBackPressedFromFragment();
            }
        });
        playPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPause.setBackgroundResource(R.drawable.play_pause_button);
                if(isplaying){
                    //pauseMusic();
                    listener.pauseMusic();
                    isplaying = false;
                    playPause.setImageResource(R.drawable.play);
                }else{
                    //resumeMusic();
                    listener.resumeMusic();
                    isplaying = true;
                    playPause.setImageResource(R.drawable.pause);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onNextClicked();
            }
        });

        return V;
    }

    public interface fragmentListener{
        void pauseMusic();
        void resumeMusic();
        void onBackPressedFromFragment();
        void startMusicPlayerService();
        void onNextClicked();
    }


    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);
        Log.d("MusicServiceFragment","OnAttach Fragment called..");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("MusicServiceFragment","OnStart Fragment called..");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("MusicServiceFragment","OnResume Fragment called..");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("MusicServiceFragment","OnPause Fragment called..");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("MusicServiceFragment","OnStop Fragment called..");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("MusicServiceFragment","OnDestroyView called..");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("MusicServiceFragment","OnDetach called..");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MusicServiceFragment","OnDestroy Fragment called..");
    }

}